#Basado en https://www.codementor.io/garethdwyer/building-a-telegram-bot-using-python-part-1-goi5fncay

import requests
import random
import json
import time
import urllib

TOKEN = "SU TOKEN" #@BotFather, Nuevo Bot y obtiene el Token
URL = "https://api.telegram.org/bot{}/".format(TOKEN)
TIMEOUT = "100"

def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js

#Obtiene todos los updates nuevos
def get_updates(offset=None):
    url = URL + "getUpdates?timeout={}".format(TIMEOUT)
    #url = URL + "getUpdates?"
    if offset:
        url += "&offset={}".format(offset)
    js = get_json_from_url(url)
    return js

#Obtiene el ID del ultimo update
def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)

#Obtiene el ultimo update de todos los mensajes nuevos
def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)

#Procesa la respuesta a todos los mensajes
def echo_all(updates):
    for update in updates["result"]:
        text = update["message"]["text"]
        chat = update["message"]["chat"]["id"]
        send_message(text,chat)

#Se envia el mensaje
#En este momento se reenvia justo el mensaje que ingreso el user
def send_message(text, chat_id):
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)

#Recibe y responde a los nuevos mensajes, y elimina los ya procesados 
def main():
    last_update_id = None
    while True:
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            echo_all(updates) 


if __name__ == '__main__':
    main()

